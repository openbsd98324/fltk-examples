# fltk-examples


## Library 
pkgin install fltk 

or 

apt-get install libfltk1.3-dev clang 



## NetBSD 

To compile: 

`` 
 c++   -lm     -I"/usr/X11R7/include/" -I"/usr/pkg/include" -I /usr/pkg/include/    -L"/usr/pkg/lib"  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk      flfilecopy.cxx         -o    flfilecopy       ;  ./filecopy 
`` 



## Debian

`` 
  g++ -lfltk -lX11 table-simple.cxx  -o table-simple ; ./table-simple  
`` 


## FLUID for Graphical Design

Fluid in action : 
![](screenshot/design.png)

![](screenshot/fltimezone.png)





